1)Install project locally:

 - git clone https://gitlab.com/pi56_mvo/cdiscount-parse.git

 - cd cdiscount-parse

2)Install NPM packages(npm: 6.11.3, node: v12.6.0):

 - npm install
 
3)Run scrapper:

 - node app.js