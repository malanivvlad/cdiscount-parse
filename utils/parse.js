const Driver = require('./driver')
const {By, until} = require('selenium-webdriver');
const config = require("../config");
const fs = require('fs');


exports.parse = async function parse(err) {
  let csv = "'name', 'url', 'price', 'description'\n";
  if (err) console.log(err);
  let driver = await Driver.getDriver();
  try {
    await driver.get(config.target_url);
    let cookies = await driver.findElement(By.id("_evidon-accept-button"));
    // if (cookies)
    //   await driver.executeScript('window.scrollTo(0, 400)');

    await driver.wait(until.elementLocated(By.id('searchInput')), 15000);
    await driver.findElement(By.id('searchInput')).sendKeys(config.input_value);
    let searchForm = await driver.findElement(By.id('SearchForm'))
    ;
    await searchForm.findElement(By.tagName('button')).click();
    //
    await driver.sleep(5000);
    do {
      await driver.wait(until.elementLocated(By.css(config.list)), 15000)
      let list = await driver.findElements(By.css(config.list))
      for (let listInd in list) {
        let sku = await list[listInd].getAttribute("data-sku");
        if (!sku) continue;
        let description = await list[listInd].findElement(By.css(config.item_descr)).getText();
        let name = await list[listInd].findElement(By.css(config.item_name)).getText();
        let url = await list[listInd].findElement(By.css(config.item_url)).getAttribute("href");
        let priceTmp = await list[listInd].findElement(By.css(config.item_price)).getText();
        let price = parseFloat(priceTmp.replace("€", "."))
        let row = `'${name}','${url}','${price}','${description}'\n`;
        csv += row;
        console.log(sku)
      }
      let prevURL = await driver.getCurrentUrl()
      await driver.executeScript("document.querySelector('" + config.next_page + "').click()");
      await driver.wait(async () => {
        try {
          await list[0].getAttribute("data-sku");
          return false;
        } catch (e) {
          return true;
        }
      }, 30000);
      let currPage = await driver.getCurrentUrl();
      if (currPage === prevURL) {
        console.log("last page")
        break;
      }
    } while (true)
  } catch
    (e) {
    console.log(e.message);
  }
  console.log(`finished: ${csv}`);
  fs.writeFile('csv.csv', csv, function (err) {
    if (err) throw err;
    console.log('Saved!');
    process.exit(0);
  });
}

