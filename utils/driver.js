const {Builder} = require('selenium-webdriver');
let driver;

async function initialize() {
  driver = await new Builder().forBrowser('firefox').build(); // log build driver
  return driver;
}

exports.getDriver = async function getDriver() {
  if (!driver) driver = await initialize()
  return driver
}
